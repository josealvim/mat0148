\chapter{Introdução}
    A noção de \emph{forcing} foi introduzida por Paul Cohen, em 1963, 
    com o objectivo de provar a independência do Axioma da Escolha e da 
    Hipótese do Contínuo relativa à \ZF. A teoria de \emph{modelos a 
    valores booleanos}, foi introduzida por Dana Scott, com adições de 
    Robert M. Solovay e Petr Vop\v enka --- entre outros. Trata-se de 
    uma opção equivalente ao forcing, porém de mais fácil intepretação 
    devido à forma que a relação de validade de fórmulas usa da 
    estrutura de álgebra dos valores do espaço de nomes.

    Recordemo-nos da definição de estruturas de Tarski e a relação 
    de validade de fórmulas --- de uma língua compatível ---:

    {
        \renewcommand{\A}{\mathfrak{A}}
        $$ \A \vDash_\sigma \varphi $$
        
        Que diz justamente que $\varphi$ vale em $\A$ sob a 
        valoração $\sigma$, que pode ser omitida em se 
        tratando de sentenças.
    }

    Sabemos que os sistemas dedutivos, como dedução natural \etc 
    e a semântica tarskiana das estruturas de línguas de primeira 
    ordem estão relacionadas pelos teoremas de Correção e 
    Completude. Sabemos também que, para teorias suficientemente 
    complexas em línguas de primeira ordem temos os teoremas de 
    Gödel de incompletude.

    Desta forma, nossa metateoria prova que existem modelos não 
    trivialmente distintos de teorias como a Aritimética ou mesmo
    \ZF. Faz sentido, portanto, indagar sobre como produzir estes 
    --- alguns deles, pelo menos --- para produzir provas de 
    independência e consistência relativa de certas propriedades.

    \ZF é plenamente capaz de fazer exatamente isso, o universo 
    construtível testemunha a equiconsistência de \ZF com 
    \ZF+$(V=L)$, em particular, a consistência relativa de GCH e 
    ainda mais particular, AC. No entanto, não havia, até então 
    nenhum modelo transitivo como $L$ que pretendia testemunhar 
    a negação de GCH ou CH, por exemplo.

    \section{Um breve sumário da teoria} {
        Devemos notar que este método --- e muitos dos resultados 
        que se obtém do mesmo --- pode ser feito para estruturas 
        algébricas muito mais gerais: álgebras de Heyting completas, 
        por exemplo, são suficientes para obter modelos de I\ZF.
        
        Como queremos modelos clássicos de $\ZF$, não vamos tratar 
        de modelos a valores em álgebras de Heyting completas \etc

        O cerne da teoria de modelos a valores booleanos de conjuntos é 
        a ideia de \emph{espaço de nomes}. Conjuntos carregam --- por 
        extensionalidade --- apenas a informação de seus membros: um 
        conjunto é unicamente determinido pela sua classe de funções 
        características, isto é:
     
        \newcommand{\K}{\mathcal{K}}

        $$\K(x) = \SET{f : \exists D: x\subseteq D \land (f : D\to 2) \land f(y) = 1 \bim y\in x} $$
        
        E toda classe maximal de funções compatíveis com codomínio $2$
        com união com suporte existente define um único conjunto $x$ 
        que tem esta classe como $\K(x)$.

        Isso é bom, na medida que membros de uma classe $\K(x)$ são 
        representantes de um objeto qualquer. A generalização que é 
        primeiro: tentar remover os conjuntos inteiramente da 
        definição, note que acima usamos subconjuntos contendo o 
        domínio, e a relação de pertinência com os membros; segundo:
        não usar funções com codomínio $2$.

        Remover os conjuntos subjacentes das supostas funções características 
        nos permitiria tratar de objetos genéricos. Ao mudar o codomínio das 
        funções, quebramos alguns elementos da semântica tradicional sem perder 
        o resto, e produzimos modelos diferentes de $\ZF$.

        Deixe $B$ álgebra booleana completa, para cada $\alpha$ ordinal deixe:

        $$V^{(B)}_\alpha=\SET{x:Func(x)\land Im(x)\subseteq B\land\exists\beta<\alpha:Dom(x)\subseteq V^{(B)}_{\beta}},$$
        $$V^{(B)}       =\SET{x:\exists\alpha: x\in V^{(B)}_\alpha}.$$

        Esta definição recursiva garante que cada membro de $V^{(B)}$ 
        é uma função que toma valor sobre outras funções semelhantes 
        e assume valor em $B$.

        Queremos uma relação de igualdade que siga as leis de Leibniz.
        Mas, sob a ótica da igualdade ``$=$'' de \ZF, duas 
        representações do mesmo conjunto podem ser diferentes 
        (uma ser extensão da outra, por exemplo).

        Podemos definir uma interpretação para fórmulas da língua de 
        \ZF em $B$ para $V^{(B)}$, que chamamos $\bvalue{\varphi}_B$, 
        que será explorada mais tarde. Mas dispondo da mesma, podemos 
        definir uma relação alternativa de validade $\vDash$ que diz 
        que um determinado modelo a valores booleanos em $B$ satisfaz
        uma fórmula exatamente quando ela é designada o valor máximo 
        de $B$.
        
        {
            Essa nova relação de validade é um tanto estranha: apesar de
            satisfazer
        
            \renewcommand{\A}{\mathfrak{A}}
            
            \begin{enumerate}
                \item $\A\vDash_\sigma\varphi$ e $\A     \vDash_\sigma\psi$ $\iff$ $\A    \vDash_\sigma\varphi\land\psi$
                \item $\A\vDash_\sigma\forall x:\varphi(x)$                 $\iff$ para todo  $X$, $\A\vDash_{\sigma[x/X]}\varphi(x)$
                \item $\A\vDash_\sigma\exists x:\varphi(x)$                 $\iff$ para algum $X$, $\A\vDash_{\sigma[x/X]}\varphi(x)$
                \item $\A\vDash_\sigma\neg\varphi$                          $\RAR$ $\A\not\vDash_\sigma\varphi$
                \item $\A\vDash_\sigma\varphi$ ou $\A    \vDash_\sigma\psi$ $\RAR$ $\A    \vDash_\sigma\varphi\lor\psi$
                \item $\A\vDash_\sigma\psi$ ou $\A\vDash_\sigma\neg\varphi$ $\RAR$ $\A    \vDash_\sigma\varphi\rar\psi$
            \end{enumerate}

            Não é o caso em que valha as reciprocas dos últimos itens.
            No entanto, esta semântica não é trivial, ela só é um pouco 
            mais dura que a tarskiana pois não admite que de uma 
            disjunção possamos quebrar uma prova em duas subprovas; ou 
            que provar a não-validade de algo seja equivalente a provar 
            a invalidade do mesmo.

            Isso pode ser facilmente entendido considerando que, apesar 
            de clássica --- $\varphi\lor\neg\varphi$ é válido ---, não 
            temos que $\varphi$ é $1$ ou $\neg\varphi$ é $1$. Pois isso 
            (exceto em $2$) \emph{nunca} é verdade em álgebras de Heyting 
            (e, portanto, Boole).

            Todas as instâncias de tautologia da lógica proposicional, 
            teoremas da lógica de primeira ordem continuam válidas, 
            \modusponens{} continua, inferência válida. Essencialmente, 
            o grosso da semântica tarskiana continua válido, no caso, 
            temos o teorema da correção.

            Com correção já podemos dizer que se um modelo não valida 
            uma determinada fórmula, então não pode ser que ela é um 
            teorema de \ZF.
        }
    }

    \section{Nossos Resultados} {
        Na literatura temos descrita uma forma de obter uma função $\V\A\to\V\B$ 
        dado um morfismo completo injetor $\A\to\B$ que satisfaz:

        $$ \varphi\bvalue{x\in y} \leq \bvalue{\tilde\varphi(x)\in\tilde\varphi(y)} $$

        Encontramos uma generalização, em certo aspecto, do que foi feito na 
        literatura. Fomos capazes de produzir, dado qualquer morfismo de locales 
        uma relação subclasse de $\V\A\times\V\B$ com domínio $\V\A$ que 
        satisfaz a exigência sobre as semânticas. A igualdade vale para fórmulas 
        $\Delta_0$, trivialmente e 

        Nas flechas da categoria dos modelos a valores booleanos existe uma estrutura 
        de ordem, e queremos saber se a relação que produzimos é maximal. A função 
        definida na literatura está contida --- como relação --- na que definimos, 
        então é uma extensão de fato: encontramos mais objetos que fazem o serviço. 
        
        Queremos, também, saber se temos um funtor de fato entre a categoria das 
        álgebras booleanas completas com flechas morfismos de locales para uma 
        determinada categoria de modelos a valores booleanos.
    }