\section{Ordens Parciais e Reticulados:}

\newcommand{\lat}{\TUPLE{\mathbf{L}}{\leq}}
\newcommand{\CS}[1]{S({#1})}
\newcommand{\CI}[1]{I({#1})}
\newcommand{\hnd}{\land}
\newcommand{\hor}{\lor}
\newcommand{\bb}{\mathbf}

\begin{definition}{(Relação de) Ordem Parcial.}    
    $R$ é chamado de relação de ordem parcial sobre $C$ se, e somente se, valem todas:

    \begin{enumerate}[label=\roman*)]
        \item $R \subseteq C\times C$.
        \item $a \in C \rar  \TUPLE{a}{a}\in R$.                           (reflexiva)
        \item $\TUPLE{a}{b}, \TUPLE{b}{a}\in R \rar a = b$.                (anti-simétrica)
        \item $\TUPLE{a}{b}, \TUPLE{b}{c}\in R \rar \TUPLE{a}{c}\in R$.    (transitiva)
    \end{enumerate}
    
    Por clareza, abreviamos ``$\TUPLE{a}{b}\in R$'' para, simplesmente,
    ``$a\mathrel{R}b$'', que lê-se ``a relação b''.
\end{definition}

\begin{definition}{Poset (Conjunto parcialmente ordenado).}
        Um conjunto $L$ é dito um \emph{poset} se, e somente se:

    \begin{enumerate}[label=\space]
        \item i) $L = \TUPLE{\mathbf{L}}{\leq}$.
        \item ii) $\leq$ uma relação de ordem parcial sobre $\mathbf{L}$.
    \end{enumerate}

\end{definition}

A intuição é que $\leq$ dispõe os elementos de $S$ em uma espécie de rede, que flui 
de baixo para cima, onde dois pontos $x$, $y$ estão ``conectados'' (por potencialmente 
outros pontos que formam uma sorte de fio entre $x$ e $y$) se $x\leq y \lor y\leq x$. E
$x$ está ``abaixo'' de $y$ se $x\leq y$.

Essa analogia pode ajudar a compreender a natureza de um \emph{poset}, mas tem seus 
limites: um poset não tem obrigação nenhuma de ser finito, limitado, ou de outra maneira 
decente, a depender da ordem.

De fato, não há necessidade de Poset ser ``conexo'' nestes 
termos, podem haver tantos fios quanto se queira que simplesmente não tem nada em comum, 
isto pois a relação de ordem parcial é bem flexível. Por exemplo: pode ser o caso de que
$\neg(x\leq y \lor y\leq x)$, neste caso, não existiria ``ancestral comum'' entre $x$ e 
$y$, tampouco um $z$ que tenha como ``ancestrais'' ambos $x$ e $y$. Seriam parte de duas 
``populações'' disconexas.

\begin{definition}{Ínfimo e Supremo sob a ordem de $\leq$.}
    Seja $\TUPLE{S}{\leq}$ um poset. Dado $M\subseteq S$, 
    definimos a propriedade de $a\in S$ ser uma 
    ``cota superior'' de $M$ por:
    $$\forall m\in M, m\leq a$$

    Quanto a $a\in S$ ser ``cota inferior'', definimos semelhantemente:        
    $$\forall m\in M, a\leq m$$

    Chamamos:

    \begin{enumerate}[label=\space]
        \item i) $\CS{M} =_{def}  \{ c\in S : \forall m\in M, m\leq c \}$.
            O conjunto das cotas superiores.
        \item ii) $\CI{M} =_{def} \{ c\in S : \forall m\in M, c\leq m \}$.
            O conjunto das cotas inferiores.
    \end{enumerate}
    Assim, definimos $s\in S$ ser ``supremo'' de M e $i\in S$ ser ``ínfimo'' do mesmo (ambos com respeito a $\leq$):
    \begin{center}
        $s$ é \emph{supremo} de $M$ com respeito a $\leq$ $\bim$ $s\in\CS{M}\land\forall c\in\CS{M}: s\leq c$.\\
        $i$ é \emph{ínfimo } de $M$ com respeito a $\leq$ $\bim$ $i\in\CI{M}\land\forall c\in\CI{M}: c\leq s$.\\
    \end{center}

    Abreviaremos $x$ ter a propriedade de ser supremo de $M$ com respeito a $\leq$ por ``$x$ $sup$ $M$'', 
    note que omitimos a ordem, se sentirmos que pode haver ambiguidade, escreveremos ``$x$ $sup_\leq$ $M$''.
    Fazemos o mesmo para o ínfimo: $x$ ter a propriedade de ser ínfimo de $M$ torna-se ``$x$ $inf$ $M$''.

    Quando provarmos que se um conjunto possuír supremo (ínfimo) este será único, usaremos a notação mais 
    razoável: $x=\sup(M)$ ou $x=\inf(M)$.
\end{definition}

Dado um $\TUPLE{\mathbf{L}}{\leq}$ reticulado, um determinado subconjunto 
$M\subseteq\mathbf{L}$ não precisa possuir qualquer cota superior ou inferior, 
e mesmo que os tenha, não é necessário que existam supremos ou ínfimos. De 
fato, e aqui pedimos a crença do leitor, $\Z$ com a ordem usual é parcialmente, 
ordenado. E verifica-se que, se $M$ é subconjunto infinito de $\Z$, $M$ 
não pode ter ambas cotas $\CS{M}$ $e$ $\CI{M}$ não vazias, isso porque M é 
obviamente ilimitado. Por outro lado, dado qualquer irracional $i\in\R$ 
Real de Dedekind, $i$ é um subconjunto dos racionais que, claramente não 
alcança supremo algum em $\Q$ mas é facilmente cotado superiormente.

\begin{theorem}{Supremos e Ínfimos são Únicos}
    Em outras palavras:

    Seja $$\TUPLE{\mathbf{L}}{\leq}$$ um reticulado e $M\subseteq\mathbf{L}$
    \begin{enumerate}[label=\roman*)]
        \item $[\exists s\in M: s$ $sup$ $M]\rar[\exists! s\in M: s$ $sup$ $M]$.
        \item $[\exists i\in M: s$ $inf$ $M]\rar[\exists! s\in M: s$ $inf$ $M]$.
    \end{enumerate}
    \begin{proof}

        Se existe algum, tome dois, não necessáriamente distintos, $l$ e $l'$ tais que:
        \begin{prooftree}
            \AxiomC{$l'$ $sup$ $M$}
            \AxiomC{$l$ $sup$ $M$}
            \UnaryInfC{$l\in\CS{M}$}
            \BinaryInfC{$l'\leq l$}

            \AxiomC{$l$ $sup$ $M$}
            \AxiomC{$l'$ $sup$ $M$}
            \UnaryInfC{$l'\in\CS{M}$}
            \BinaryInfC{$l\leq l'$}

            \BinaryInfC{$l=l'$}
        \end{prooftree}
        Omitiremos a prova para o ínfimo, pois ela é muito parecida.
    \end{proof}
\end{theorem}

Com essa prova, podemos agora, falar \emph{do} supremo um conjunto, 
como faríamos com uma noção de aplicação.

Finalmente, agora que temos a unicidade garantida,
podemos definir operadores parciais nas partes do conjunto:

Dado um $K\subseteq S$, $\langle S,\leq\rangle$ um poset, abreviamos
``$x$ $sup$ $K$'' para ``$x = \sup(K)$'', similarmente, 
``$x$ $inf$ $K$'' torna-se ``$x = \inf(K)$''.

Obviamente, $inf$ e $\sup$ ainda são referentes a uma ordem em
particular. Assim, se houver ambiguidades, seremos forçados a
escrever ${\sup}_\leq(K)$.

Tendo essa noção de \emph{supremo} e \emph{ínfimo}, podemos nos focar numa estrutura
rica no sentido de que ela se comporta bem sob estes operadores de supremo e ínfimo.

\begin{definition}{Reticulado}
    $P$ é dito um Reticulado se, e somente se, são verdades:

    \begin{enumerate}[label=\roman*)]
        \item $P = \TUPLE{S}{\leq}$ é um poset.
        \item $\forall a, b\in S:\exists s\in S : s = \sup\{a, b\}$.
        \item $\forall a, b\in S:\exists i\in S : i = \inf\{a, b\}$.
    \end{enumerate}
    
    Este reticulado é dito completo exatamente quando:
    $$\forall K\subseteq S:\exists s, i\in S : s = \sup(K) \land i = \inf(K)$$
    
    Este reticulado é dito limitado exatamente quando:
    $$\exists 0,1\in S:\forall a\in S: 0\leq a\leq 1$$

    Neste caso, tratamos $P$ como reticulado também, pois, se esquecermos de $1,0$
    ele passa a ser exatamente isso. \footnote{
        Alguns escolhem escrever os
        reticulados limitado como 
        $\TUPLE{X}{\leq}{1}{0}$, mas veremos
        que se $\TUPLE{X}{\leq}$ for limitado,
        $1$ e $0$, são únicos, e a condição
        de existência é só contingente à 
        ordem, então por vezes omitiremos partes 
        das tuplas, pois elas não carregam 
        informação sobre a estrutura que não
        possamos recuperar de outra forma.

        A assinatura cheia é útil para definir morfismos, \etc.
    }

    P, um reticulado, é dito Distributivo se, e somente se vale:
    \begin{enumerate}[label=\alph*)]
        \item  $\forall a,b,c\in\bb{H}:\inf\{a,\sup\{b,c\}\}=\sup\{\inf\{a,b\},\inf\{a,c\}\}$.
        \item  $\forall a,b,c\in\bb{H}:\sup\{a,\inf\{b,c\}\}=\inf\{\sup\{a,b\},\sup\{a,c\}\}$.
    \end{enumerate}
\end{definition}

\begin{theorem}{\emph{Extrema} de subconjunto finito de reticulado limitado.}
    
    Seja $L = \TUPLE{S}{\leq}{1}{0} $ um reticulado limitado, 
    para todo conjunto finito $K\subseteq S$ existe um supremo e um ínfimo de $K$ em $S$.

    \begin{proof}
        Provaremos por indução sobre o tamanho de K:
        Para $|K| = 0$: $\sup(\emptyset)=0$.

        Isso pois, $\forall x\in\emptyset:x\leq 0$ 
        e $\forall x\in S: 0\leq x$.

        Adicionalmente, $\inf(\emptyset) = 1$, pois 
        $\forall x\in\emptyset, 1 \leq x$ e, similarmente,
        não existe maior cota inferior que $1$.

        Para conjuntos unitários $X=\{x\}\subseteq S$,temos, 
        trivialmente, $\sup(X)=\inf(X)=x$.

        Já para conjuntos binários \{x, y\}, a 
        condição de reticulado nos garante existência de
        \emph{extrema}.

        Agora, suponha que 
        $\forall K\subseteq S:|K|\leq n \rar \exists s\in S : s = \sup(K)$.
        agora, tome algum $K' = K\cup \{x\}$ com $x\in S$,
        provaremos que:

        \begin{enumerate}[label=\roman*)]
            \item $\sup(K') = \sup\{\sup(K), x\}$.
            \item $\inf(K') = \inf\{\inf(K), x\}$.
        \end{enumerate}
        
        Primeiro, provaremos para o sup, para tal, é suficiente provar que:
        $$\CS{K'} = \CS{\{x,\sup(K)\}}.$$

        Pois sabemos que $\{x,\sup(K)\}$ tem sup pela condição de reticulado, e, sabemos que a existência de
        sup implica unicidade, pelo Teorema 2.1.

        \begin{prooftree}
            \AxiomC{$c\in\CS{\{x,\sup(K)\}}$}
            \UnaryInfC{$\sup(K)\leq c \land x\leq c$}
            \UnaryInfC{$\forall k\in K: (k\leq c)\land (x\leq c)$}
            \UnaryInfC{$\forall k'\in K': (k\leq c)$}
            \UnaryInfC{$c\in\CS{K'}$}
        \end{prooftree}

        Do outro lado,
        \begin{prooftree}
            \AxiomC{$c\in\CS{K'}$}
            \UnaryInfC{$\forall k\in K': k\leq c$}
            \UnaryInfC{$\forall k'\in K: k\leq c \land x\leq c$}
            \UnaryInfC{$c\in\CS{K}\land x\leq c$}
            \UnaryInfC{$\sup(K)\leq c, \land x\leq c$}
            \UnaryInfC{$c\in\CS{\{x,\sup(K)\}}$}
        \end{prooftree}
        Assim:
        \begin{center}
            $c\in\CS{K'}\bim c\in\CS{\{x,\sup(K)\}}$\\
            $\therefore \CS{K'} =\CS{\{x,\sup(K)\}}$.\\
        \end{center}

        Concluindo: como as cotas superiores são idênticas e sabemos 
        que $\{x,\sup(K)\}$ tem sup, está provado o passo indutivo do
        sup.

        A prova para ínfimo tem o mesmo espirito que a descrita acima
        e acreditamos que não será necessário delinea-la aqui.
    \end{proof}
\end{theorem}

\begin{corollary} 
    Em um reticulado limitado, ``0'' e ``1'' são ambos únicos.

    Trivial
\end{corollary}

Por vezes, denotamos $0$ por $\bot$ e $1$ por $\top$, e lemos 
--- respectivamente, \emph{bottom} e \emph{top}.

Convém definir, dado um Poset $P=\TUPLE{S}{\leq}$, um par 
de operadores, $\land$ e $\lor$, (distintos dos conectivos
lógicos, mas com mesmo símbolo). $\land:S\times S\to S$, 
$\lor :S\times S\to S$, tais que $a\land b = \inf\{a,b\}$
e $a\lor b = \sup\{a,b\}$
    
Pela definição, é fácil ver que ambos operadores são 
crescentes, nas duas variáveis e que vale a comutatividade.

\begin{theorem}{$\hnd$ e $\hor$ são Associativos}
    \begin{proof}
        Fixe $\alpha,\beta,\gamma\in\mathbf{L}$, $\TUPLE{\mathbf{L}}{\leq}$ um reticulado.

        \begin{prooftree}
            \AxiomC{seja $c\leq\alpha\hnd(\beta\hnd\gamma)$}
            \UnaryInfC{$c\in\CI{\{\alpha,(\beta\hnd\gamma)\}}$}
            \UnaryInfC{$c\leq\alpha$ E $c\leq\beta\hnd\gamma$}

            \AxiomC{$c\leq\beta\hnd\gamma$}
            \UnaryInfC{$c\in\CI{\{\beta,\gamma\}}$}
            \UnaryInfC{$(c\leq\beta)$ E $(c\leq\gamma)$}
            
            \BinaryInfC{$c\leq \alpha,\beta,\gamma$}
            \UnaryInfC{$c\leq(\alpha\hnd\beta)\hnd\gamma$}
        \end{prooftree}

        Assim:
        \begin{prooftree}
            \AxiomC{$c\leq\alpha\hnd(\beta\hnd\gamma)$}
            \UnaryInfC{$c\leq(\alpha\hnd\beta)\hnd\gamma$}
        \end{prooftree}

        Agora, simplesmente aplicamos este resultado para 
        $\alpha'=\gamma$, $\beta'=\beta$ e $\gamma'=\alpha$:

        \begin{prooftree}
            \AxiomC{$c\leq(\alpha\hnd\beta)\hnd\gamma$}         \RightLabel{, comutando}
            \UnaryInfC{$c\leq(\beta\hnd\alpha)\hnd\gamma$}      \RightLabel{, substituindo}
            \UnaryInfC{$c\leq(\beta'\hnd\gamma')\hnd\alpha'$}   \RightLabel{, novamente, comutando}
            \UnaryInfC{$c\leq\alpha'\hnd(\beta'\hnd\gamma')$}   \RightLabel{, pelo que provamos acima}
            \UnaryInfC{$c\leq(\alpha'\hnd\beta')\hnd\gamma'$}   \RightLabel{, susbtituindo}
            \UnaryInfC{$c\leq(\gamma\hnd\beta)\hnd\alpha$}      \RightLabel{,pela comutatividade}
            \UnaryInfC{$c\leq\alpha\hnd(\beta\hnd\gamma)$}      
        \end{prooftree}

        Seja $s = \inf\{(\alpha\hnd\beta)\hnd\gamma\}$. Claramente,

        $s=(\alpha\hnd\beta)\hnd\gamma$.
        
        Mas, por outro lado, o conjunto das cotas inferiores de
        $(\alpha\hnd\beta)\hnd\gamma$ é exatamente igual ao de 
        $\alpha\hnd(\beta\hnd\gamma)$, então, pela definição do 
        $\inf$, deve também ser o caso que 
        $s=\alpha\hnd(\beta\hnd\gamma)$.

        Ou seja: $(a \hnd b) \hnd c = a \hnd(b \hnd c)$
    \end{proof}
\end{theorem}

\begin{lemma}{Relações de $\hnd$ e $\hor$ com $\leq$}
    $$(\alpha\leq\beta)\bim(\alpha\hor\beta=\beta)$$
    $$(\alpha\leq\beta)\bim(\alpha\hnd\beta=\beta)$$
    
    Fixado um $\lat$ um Reticulado e $\alpha,\beta$ ambos em $\bb{L}$,
    \begin{prooftree}
        \AxiomC{$\alpha\leq\beta$}
        \UnaryInfC{$\alpha\in\CI{\{\alpha,\beta\}}$}
        \AxiomC{$   i=\inf\{\alpha,\beta\}$}
        \UnaryInfC{$i\leq\alpha,\beta$ E $\forall c\in\CI{\{\alpha,\beta\}}:c\leq i$}
        \BinaryInfC{$i\leq\alpha\land\alpha\leq i$}
        \UnaryInfC{$\inf\{\alpha,\beta\} = i = \alpha$}
        \UnaryInfC{$\alpha = \inf\{\alpha,\beta\}$}
        \UnaryInfC{$\alpha = \alpha\hnd\beta$}
    \end{prooftree}

    \begin{prooftree}
        \AxiomC{$\alpha = \alpha\hnd\beta$}
        \UnaryInfC{$\alpha = \inf\{\alpha,\beta\}$}
        \UnaryInfC{$\alpha\leq\alpha,\beta$}
        \UnaryInfC{$\alpha\leq\beta$}
    \end{prooftree}

    Assim, $\alpha\leq\beta\iff\alpha\hnd\beta=\alpha$.

    Para o $\hor$, a prova é muito similar, logo não a apresentaremos, mas vale que:

    $$\alpha\leq\beta\iff\alpha\hor\beta=\beta$$
\end{lemma}

\begin{definition}{A família de operadores $L_\alpha$.}
    Fixado $\lat$ um reticulado, definimos a família $L_\alpha$ de operadores em $\bb{L}$
    \begin{align*}
        L_\alpha:\bb{L}&\to\bb{L}\\
        \beta&\mapsto\alpha\hnd\beta
    \end{align*}
\end{definition}

\begin{lemma}{$L_\alpha$ é crescente.}
    $$\forall\alpha\in\bb{L}:i\leq j\rar \alpha\hnd i\leq\alpha\hnd j$$

    \begin{prooftree}
        \AxiomC{$\beta\leq\beta'$}
        \UnaryInfC{$\beta\hnd\beta' = \beta$}
        
        \AxiomC{$k =_{def}L_\alpha(\beta)\hnd L_\alpha(\beta')$}
        \UnaryInfC{$k = (\alpha\hnd\beta)\hnd(\alpha\hnd\beta')$}
        \UnaryInfC{$k = (\alpha\hnd\alpha)\hnd(\beta\hnd\beta')$}
        \BinaryInfC{$k=\alpha\hnd\beta$}
        \UnaryInfC{$k = L_\alpha(\beta)$}
        \UnaryInfC{$L_\alpha(\beta)\leq L_\alpha(\beta)$}
    \end{prooftree}
\end{lemma}


\begin{theorem}{Em reticulado valem as leis absorção}
    $L = \lat$ Reticulado. $a,b\in\bb{L}$
    \begin{enumerate}[label=\roman*)]
        \item $a\hnd(a\hor b) = a$.
        \item $a\hor(a\hnd b) = a$.
    \end{enumerate}
    \begin{proof}
        \begin{subproof}$ $%ABSOR 1
            \begin{prooftree}
                \AxiomC{$a\hnd(a\hor b) \leq a$}
                \AxiomC{$a\leq a\hor b$}
                \UnaryInfC{$L_a(a)\leq L_a(a\hor b)$}
                \UnaryInfC{$a\leq a\hnd(a\hor b)$}
                \BinaryInfC{$a = a\hnd(a\hor b)$}
            \end{prooftree}    
        
        \end{subproof}
        \begin{subproof}$ $%ABSOR 2
            \begin{prooftree}
                \AxiomC{$ a\hor(a\hnd b)\leq a$}
                \AxiomC{$ a\leq (a\hnd b)$}
                \UnaryInfC{$a\hor a\leq a\hor(a\hnd b)$, pois $\hor$ é claramente crescente}
                \UnaryInfC{$a\leq a\hor(a\hnd b)$}
                \BinaryInfC{$a = a\hor(a\hnd b)$}
            \end{prooftree}
        
        \end{subproof}        
    \end{proof}
\end{theorem}

\begin{theorem}{Resultados para reticulados.}
    Seja $L = \TUPLE{\bb{L}}{\leq}$ um reticulado e 
    $a,b,c\in\bb{L}$. Vale o que segue:

    \begin{enumerate}[label=\roman*)]
        \item $a\leq b \iff a\hnd b = a$.
        \item $a\leq b \iff a\hor b = b$.

    \end{enumerate}    
    \begin{proof}
        \begin{subproof}
            \paragraph{$(\rar)$:}
            \begin{prooftree}
                \AxiomC{$a\hnd b\leq a$}
                
                \AxiomC{$a\leq b$}
                \UnaryInfC{$L_a(a)\leq L_a(b)$}
                \UnaryInfC{$a\hnd a\leq a\hnd b$}
                \UnaryInfC{$a\leq a\hnd b$}
                
                \BinaryInfC{$a=a\hnd b$}
            \end{prooftree}
            Agora, para a volta,

            \paragraph{$(\lar)$:}
            \begin{prooftree}
                \AxiomC{$a\hnd b\leq b$}
                \AxiomC{$a\hnd b = a$}
                \UnaryInfC{$a\leq a\hnd b$}
                \BinaryInfC{$a\leq b$}
            \end{prooftree}
            
        \end{subproof}

        \begin{subproof}
            \paragraph{$(\rar)$:}
            \begin{prooftree}
                \AxiomC{$a\leq b$}
                \UnaryInfC{$a\hor b= (a\hnd b)\hor b$}
                \UnaryInfC{$a\hor b= b$}
            \end{prooftree}

            \paragraph{$(\lar)$:}
            \begin{prooftree}
                \AxiomC{$a\hor b = b$}
                \UnaryInfC{$(a\hor b)\hnd a= b\hnd a$}
                \UnaryInfC{$a= b\hnd a$}
                \UnaryInfC{$a\leq b$}
            \end{prooftree}
            
        \end{subproof}
    \end{proof}
\end{theorem}

Com isso, concluímos o trato de reticulados diretamente. Mas antes de começarmos 
com Álgebras de Heyting, faremos um adendo.

\input{source/chapter-0/section/lattices/subsection/topology}