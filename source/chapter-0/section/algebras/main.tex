\section{Algebras de Heyting e Boole}

\newcommand{\hey}{\TUPLE{\bb{H}}{\leq}}
\newcommand{\hop}{\rar}

Conhecemos bem um exemplo de álgebra Booleana, 
esta é a chamada álgebra do $2$, ou álgebra de
verdades, bem conhecida por qualquer um que 
estudou um computação em alguma medida, já que
saber como se comportam os valores 
{\bf booleanos}, \texttt{true} e 
\texttt{false}, é importante para usar \emph{
condicionais}, que são frequentes nas línguagens
de programação.

Álgebras Booleanas se relacional com a lógica
clássica, e são modelos de lógicas com \emph{
aspectos} clássicos: leis de De Morgan, 
$\yields\psi\lor\neg\psi$, eliminação da dupla
negação, \etc.

A estrutura de álgebra Booleana é uma 
generalização das propriedades da álgebra do 
$2$, e estudaremos elas mais a fundo depois do 
nosso trabalho preliminar com as álgebras de 
Heyting.
    
Uma Álgebra de Heyting é uma estrutura álgebrica
próxima da álgebra Booleana, mas com menos 
restrições feitas à estrutura. Elas se 
relacionam, como veremos, com a lógica 
intuícionista.

\begin{definition}{Álgebras de Heyting}
    Dizemos\footnote{
        Alguns podem querer escrever $H=\TUPLE{\bb{H}}{\leq}{\hop}$, mas como veremos, $\hop$ está
        determinada pela ordem, então ela não carrega adicional nenhuma informação quanto a estrutura.
        Quando trabalharmos com 
        morfismos, vamos ``crescer'' nossas estruturas, $H=\TUPLE{\bb{H}}{\leq}{\hop}{\hnd}{\hor}{1}{0}$, para 
        definir morfismos. Mas para definições e preliminares, achamos mais 
        produtivo tratar das nossas álgebras como \emph{reticulados muito específicos} ao invés de uma 
        estrutura de tipo próprio.
    } $H=\TUPLE{\bb{H}}{\leq}$ ser uma Álgebra de Heyting quando valem todas:

    \begin{enumerate}
        \item $\langle{\bb H},\leq\rangle$ é um reticulado limitado.
        \item $\hop:{\bb H}\times{\bb H}\longrightarrow{\bb H}$ é um operador.
            \footnote{escreve-se infixo e lê-se $\alpha\hop\beta$: ``$\alpha$ implica $\beta$''}
        \item $a\hop a = 1$.
        \item $a\hnd (a\hop b) = a\hnd b$.
        \item $b\hnd (a\hop b) = b$.
        \item $a\hop (b\hnd c) = (a\hop b)\hnd(a\hop c)$.
        \item $\min \bb H \not= \max \bb H$
    \end{enumerate}
\end{definition}

\begin{lemma}{\em $\hop$ é crescente na segunda variável}
    Fixada uma $H = \TUPLE{\bb{H}}{\leq}$ Álgebra de Heyting, 

    Começamos definindo outra família indexada de aplicações unárias sobre $\bb{H}$,
    Dado $\alpha\in\bb H$:
    \begin{align*}
        R_\alpha:\bb{H}&\to\bb{H}\\
        \beta&\mapsto(\alpha\hop\beta)
    \end{align*}

    Ela é crescente.
    \begin{proof}
        \begin{prooftree}
            \AxiomC{$\beta\leq\beta'$}
            \UnaryInfC{$\beta\hnd\beta' = \beta$}
            
            \AxiomC{$k =_{def}R_\alpha(\beta)\hnd R_\alpha(\beta')$}
            \UnaryInfC{$k =(\alpha\hop\beta)\hnd(\alpha\hop\beta')$}
            \UnaryInfC{$k =\alpha\hop(\beta\hnd\beta')$}
            
            \BinaryInfC{$k=\alpha\hop\beta$}
            \UnaryInfC{$k = R_\alpha(\beta)$}
            \UnaryInfC{$R_\alpha(\beta)\leq R_\alpha(\beta')$}
        \end{prooftree}
        Concluindo,

        $$\beta\leq\beta'\rar R_\alpha(\beta)\leq R_\alpha(\beta)$$
    \end{proof}
\end{lemma}

\begin{theorem}{\em: Teorema do ``Passeio''}
    \hspace*{\parindent}Dada uma Álgebra de Heyting $H = \hey$,
    Vale que $(z\leq x\rightarrow y)\bim(z\land x\leq y)$.
    \begin{proof}
        \begin{prooftree}
            \AxiomC{{\em Note que sempre $\beta\leq L_\alpha(\beta)$}}
            \AxiomC{{\em $R_\alpha$ crescente}}
            \AxiomC{{\em $L_\alpha$ crescente}}
            
            \AxiomC{$z\leq x\hop y$}

            \BinaryInfC{$x\hnd z\leq x\hnd x\hop y$}
            \UnaryInfC{$x\hnd z\leq x\hnd y$}
            \UnaryInfC{$x\hnd z\leq y$}
            
            \BinaryInfC{$x\hop(z\hnd x) \leq x\hop y$}
            \UnaryInfC{$(x\hop x)\hnd(x\hop z)\leq x\hop y$}
            \UnaryInfC{$x\hop z\leq x\hop y$}
            \BinaryInfC{$z\leq x\hop y$}
        \end{prooftree}
        Logo, temos: $z\leq x\hop y\bim x\hnd z\leq y$
    \end{proof}
\end{theorem}

\begin{corollary}{Determinação da operação de implicação}
    Numa álgebra de Heyting $H=\hey$, $\hop$ está, na realidade, determinada inteiramente por $\leq$.

    Deixe $a,b\in\bb{H}$ e seja $K_{(a,b)}=\{c\in\bb{H}:c\hnd a\leq b\}$
    \begin{prooftree}
        \AxiomC{$\gamma\in K_{(a,b)}$}
        \UnaryInfC{$\gamma\hnd a\leq b$}
        \UnaryInfC{$\gamma\leq a\hop b$}

        \AxiomC{$a\hop b\leq a\hop b$}
        \UnaryInfC{$a\hnd(a\hop b)\leq b$}
        \UnaryInfC{$(a\hop b)\in K_{(a,b)}$}

        \BinaryInfC{$(a\hop b) = max(K_{(a,b)})$}
    \end{prooftree}
\end{corollary}

É fácil de se ver que dada um espaço topológico $X$, se munirmos o 
seu conjunto de abertos com a ordem da inclusão e sobre os seus pares 
definirmos a implicação $u\hop b = \bigcup \SET{ b\in\mathcal O(X): 
b\cap u\subseteq b}$ teremos uma álgebra de Heyting.

Agora, vamos provar que uma Álgebra de Heyting
é um Reticulado Distributivo, demonstrando uma
forma mais forte de distribuição, e desta 
obteremos as leis distributivas.

Usamos $\sqcup$ ou $\bigvee$ e $\sqcap$ ou $\bigwedge$ para supremos 
e ínfimos indexados, a depender da álgebra.

\begin{theorem}{Distribuição e Implicação}
    
        Em uma $H=\TUPLE{\bb{H}}{\leq}$ álgebra de Heyting
    $\forall a\in\bb{H}$, vale que:
    Para uma família indexada ${b_i: i\in I}\subseteq\bb{H}$
    \begin{enumerate}
        \item Se existir $\bigsqcup_{i\in I}b_i$ então, $a\hnd\left(\bigsqcup_{i\in I}b_i\right) = \bigsqcup_{i\in I}(a\hnd b_i)$    
        \item Se existir $\bigsqcup_{i\in I}b_i$ então, $\left(\bigsqcup_{i\in I}b_i\right)\hop a = \bigsqcap_{i\in I}(b_i\hop a)$
    \end{enumerate}

    \begin{proof}
        \begin{subproof}

            Para $a\hnd\left(\bigsqcup_{i\in I}b_i\right)=\bigsqcup_{i\in I}(a\hnd b_i)$:
            \begin{prooftree}
                \AxiomC{$\exists s\in \bb{H}: s = \bigsqcup_{i\in I}b_i$}
                
                \AxiomC{Seja $c$ cota superior de $\{a\hnd b_i\in\bb{H}:i\in I\}$}
                \UnaryInfC{$\forall i\in I: a\hnd b_i\leq c$}
                \UnaryInfC{$\forall i\in I: b_i\leq a\hop c$}
                
                \BinaryInfC{$\bigsqcup_{i\in I}b_i\leq a\hop c$}
                \UnaryInfC{$a\hnd\left(\bigsqcup_{i\in I}b_i\right)\leq c$}
            \end{prooftree}

            Ou seja, $a\hnd\left(\bigsqcup_{i\in I}b_i\right)$ é menor que qualquer cota superior. Agora:
            \begin{prooftree}
                \AxiomC{$\exists s\in \bb{H}: s = \bigsqcup_{i\in I}b_i$}
                \UnaryInfC{$\forall i\in I:b_i\leq s$}
                \UnaryInfC{$\forall i\in I:L_a(b_i)\leq L_a(s)$}
                \UnaryInfC{$\forall i\in I:a\hnd b_i\leq a\hnd\left(\bigsqcup_{i\in I}b_i\right)$}
            \end{prooftree}

            Assim, $a\hnd\left(\bigsqcup_{i\in I}b_i\right)$ é cota superior. Portanto, por definição:
            $$a\hnd\left(\bigsqcup_{i\in I}b_i\right)=\bigsqcup_{i\in I}(a\hnd b_i)$$
        \end{subproof}

        \begin{subproof} 
            Para $\left(\bigsqcup_{i\in I}b_i\right)\hop a = \bigsqcap_{i\in I}(b_i\hop a)$:
            \begin{prooftree}
                \AxiomC{$\exists s\in\bb{H}: s = \bigsqcup_{i\in I}b_i$}

                \AxiomC{Seja $c$ cota inferior de $\{b_i\hop a\in\bb{H}:i\in I\}$}
                \UnaryInfC{$\forall i\in I: c\leq b_i\hop a$}
                \UnaryInfC{$\forall i\in I: c\hnd b_i\leq a$}
                \UnaryInfC{$\forall i\in I: b_i\leq c\hop a$}
                \BinaryInfC{$\bigsqcup_{i\in I}b_i\leq c\hop a$}
                \UnaryInfC{$\left(\bigsqcup_{i\in I}b_i\right)\hnd c\leq a$}
                \UnaryInfC{$c\leq \left(\bigsqcup_{i\in I}b_i\right)\hop a$}
            \end{prooftree} 

            Isso nos dá que $\left(\bigsqcup_{i\in I}b_i\right)\hop a$ é maior que qualquer cota inferior. Agora:
            \begin{prooftree}
                \AxiomC{Seja $c$ cota inferior de $\left(\bigsqcup_{i\in I}b_i\right)\hop a$}
                \UnaryInfC{$c\leq \left(\bigsqcup_{i\in I}b_i\right)\hop a$}
                \UnaryInfC{$c\hnd\left(\bigsqcup_{i\in I}b_i\right) \leq a$}
                \UnaryInfC{$\forall i\in I:c\hnd b_i \leq a$}
                \UnaryInfC{$\forall i\in I:c \leq b_i\hop a$}
            \end{prooftree}
            
            Em particular, isso nos dá que:
            $$\left(\forall j\in I:\bigsqcup_{i\in I}b_i\right)\hop a\leq b_j\hop a$$
            
            Ou seja, $\bigsqcup_{i\in I}b_i)\hop a$ é maior cota inferior. Desse modo,
            $$\left(\bigsqcup_{i\in I}b_i\right)\hop a = \bigsqcap_{j\in I}(b_j\hop a)$$
        \end{subproof}
    \end{proof}
\end{theorem}

\begin{theorem}{Uma álgebra de Heyting $H$ é um reticulado distributivo}
    \begin{enumerate}
        \item $a\hnd(b\hor c) = (a\hnd b)\hor(a\hnd c)$
        \item $a\hor(b\hnd c) = (a\hor b)\hnd(a\hor c)$
    \end{enumerate}
    \begin{proof}
        \begin{subproof}
            Como trata-se de um reticulado, sempre existe $b\hor c=\sup\{b,c\}$, logo pelo teorema anterior,
            $a\hnd(b\hor c) = (a\hnd b)\hor(a\hnd c)$.
                    
        \end{subproof}
        
        \begin{subproof}
            \begin{prooftree}
                \AxiomC{Seja $\gamma = (a\hor b)\hnd(a\hor c)$, que é sempre possível}
                \UnaryInfC{$\gamma = (a\hnd(a\hor c) )\hor (b\hnd(a\hor c) ) $}
                \UnaryInfC{$\gamma =  a \hor (b\hnd(a\hor c)) $}
                \UnaryInfC{$\gamma =  a \hor ((b\hnd a)\hor(b\hnd c)) $}
                \UnaryInfC{$\gamma =  (a \hor (b\hnd a))\hor(b\hnd c) $}
                \UnaryInfC{$\gamma =  a\hor(b\hnd c) $}
            \end{prooftree} 
            
        \end{subproof}
    \end{proof}
\end{theorem}

Com isso em mãos, vamos agora tratar da 
\emph{pseudocomplementação}, que é o
próximo passo em direção às álgebras 
Booleanas.

A Pseudocomplementação é uma operação 
unária sobre uma Álgebra de Heyting, 
que se assemelha à negação lógica 
($\neg$). Em termos de intuição, 
queremos um elemento que é 
inteiramente incompatível com alguém,
isto é, dado um $a$, queremos achar 
um $a'$ que satisfaz $a\hnd a' = 0$.

Sempre há ao menos um, o $0$, mas 
queremos uma noção de ``melhor'' 
elemento $a'$ que satisfaz nossa 
exigência, para definir o 
pseudocomplemento de um elemento.

\begin{definition}{Pseudocomplemento}
    Dada uma Álgebra de Heyting $H = \hey$ e um elemento $e\in\bb{H}$, definimos um conjunto 
    auxiliar $K_e = \{c\in\bb{h}:c\hnd e = 0\}$.\\

    Primeiro, notemos que:
    \begin{prooftree}
        \AxiomC{$c\in K_e$}
        \UnaryInfC{$c\hnd e = 0$}
        \UnaryInfC{$c\hnd e \leq 0$}
        \UnaryInfC{$c \leq e\hop 0$}
    \end{prooftree}

    e que:
    \begin{prooftree}
        \AxiomC{$c = e\hop 0$}
        \UnaryInfC{$c \leq e\hop 0$}
        \UnaryInfC{$c\hnd e \leq 0$}
        \UnaryInfC{$c\in K_e$}
    \end{prooftree}
    
    Ou seja, $e\hop 0 = sup (K_e)$, fazendo dele o ``maior incompatível''. Seguros, agora podemos definir o operador:
    \begin{align*}
        \cdot^*:\bb{H}  &\to            \bb{H}\\
                    e   &\mapsto e^* = (e\hop 0)
    \end{align*}
\end{definition}

O pseudocomplemento de algo é o maior elemento que é 
``disjunto'' daquilo. Ou, ainda, são todas as 
testemunhas da falsidade de algo. Isso não precisa 
seguir o meio excluído, isto é: a pseudocomplementação 
não necessariamente é um complemento.

\begin{lemma}{Pseudocomplementos de $0$ e $1$}
    \begin{enumerate}
        \item $0^* = 0\hop 0 = 1$.
        \item $1^* = 1\hop 0 = 1\hnd (1\hop 0) = 1\hnd 0 = 0$.
    \end{enumerate}
\end{lemma}

Note que $1^{**} = 1$ e $0^{**} = 0$.
Esta propriedade de invariância sob
dupla complementação é importante 
pois ela mostra indica que a relação
entre um elemento e o seu complemento
é mais estrita. Chamamos esta 
propriedade de \emph{Regularidade}

Por outro lado, vemos também que:
$1^*\hor 1=1$ e que $0\hor 0^*=1$.
Esta propriedade, que nomeamos de
\emph{Complementação}, se assemelha
muito ao \emph{terceiro excluído}.
Porque apesar de ser possível haver 
mais do que três valores, o 
pseudocomplemento é de fato um complemento.

\begin{definition}{Complementação e Regularidade}
    Um elemento $a$ de uma álgebra de Heyting $H$ é dito $Complementado$ se, e só se, vale:

    \begin{enumerate}
        \item $a\hnd a^* = 0$.
        \item $a\hor a^* = 1$.
    \end{enumerate}

    Chamamos um elemento $c$ de $H$ que satisfaz, dado $a\in\bb{H}$,
    \begin{enumerate}
        \item $a\hnd c = 0$.
        \item $a\hor c = 1$.
    \end{enumerate}

    de Complemento de $a$.

    Abreviamos ``$b$ ser complemento de $a$'' para, simplesmente, ``$b$ $comp$ $a$''.
    Adiconalmente, $a\in\bb{H}$ é dito $Regular$ se, e só se, vale:

    $$a^{**} = a$$

    \begin{align*}
        Comp(H) &=_{def}\{h\in\bb{H}:h\hnd h^*=0\land h\hor h^*=1 \}.\\
        Reg(H)  &=_{def}\{h\in\bb{H}:h = h^{**}\}.
    \end{align*}

    Os chamados Complementados e os Regulares, respectivamente.
\end{definition}

\begin{theorem}{Resultados sobre Álgebras de Heyting}
    Seja $H = \hey$ uma álgebra de Heyting. E deixe $a,b\in\bb{H}$
    \begin{enumerate}
        \item $a\leq b \iff a\hop b = 1$.
        \item $a \leftrightarrow b = 1 \iff a = b$.
        \item $a\hnd a^* = 0$.
        \item $a\leq a^{**}$.
        \item $a\in Comp(H) \rar a \in Reg(H)$.
        \item $a\leq b\rar a^{**}\leq b^{**}$.
        \item $a\leq b \rar b^*\leq a^*$.
        \item $(a\hor b)^* = a^*\hnd b^*$.: De Morgan 1
        \item $(a^*\hnd b^*)^* = (a\hor b)^{**}$.: De Morgan 2 (Enfraquecida)
        \item $a^{***}= a^*$.
        \item $\exists c\in\bb{H}: c$ $comp$ $a\rar a\in Comp(H)$.
        \item $c, c'$ $comp$ $a\rar c= c'$.
        \item $a^*\hor b \leq a\hop b$.
    \end{enumerate}
    \begin{proof}
        \begin{subproof} $a\leq b \iff a\hop b = 1$.
            \begin{prooftree}
                \AxiomC{$a\hop b = 1$}
                \UnaryInfC{$ 1\leq a\hop b$}
                \UnaryInfC{$ a \leq b$}
                \UnaryInfC{$1\hnd a \leq b$}
                \UnaryInfC{$1\leq a\hop b$}
                \UnaryInfC{$1 = a\hop b$}
            \end{prooftree}
        \end{subproof}

        \begin{subproof} $a \leftrightarrow b = 1 \iff a = b$\\
            Primeiro, tratemos de definir o operador {$\leftrightarrow$} (pois o usaremos em outras subsecções):
            $$a\leftrightarrow b =_{def} (a\hop b)\hnd(b\hop a)$$
            Trivialmente, $(a\hop b) = 1$ e $(b\hop a) = 1$, mas, então,
            $$ a\leq b \leq a$$
            $$a = b$$
            
        \end{subproof}

        \begin{subproof} $a\hnd a^* = 0 $. % A^A*=0
            \begin{prooftree}
                \AxiomC{$\gamma = a\hnd a^*$}
                \UnaryInfC{$\gamma = a\hnd (a\hop 0)$}
                \UnaryInfC{$\gamma = 0$}
            \end{prooftree}
        \end{subproof}

        \begin{subproof} $a\leq a^{**}$. % A<=A**
            \begin{prooftree}
                \AxiomC{$\gamma = a\hnd a^{**}$}
                \UnaryInfC{$\gamma = a\hnd (a^*\hop 0)$}
                \UnaryInfC{$\gamma = a\hnd (a^*\hop (a\hnd a^*))$}
                \UnaryInfC{$\gamma = a\hnd (a^*\hop a)\hnd(a^*\hop a^*)$}
                \UnaryInfC{$\gamma = a\hnd (a^*\hop a)\hnd 1$}
                \UnaryInfC{$\gamma = a$}
                \UnaryInfC{$a\leq a^{**}$}
            \end{prooftree}
        \end{subproof}

        \begin{subproof} $a\in Comp(H) \rar a = a^{**}$. %Comp A -> A=A**
            \begin{prooftree}
                \AxiomC{$a\in Comp(H)$}
                \UnaryInfC{$a\hor  a^* = 1$}
                \UnaryInfC{$(a\hor  a^*)\hnd a^{**} = 1\hnd a^{**}$}
                \UnaryInfC{$(a\hnd a^{**}) \hor (a^*\hnd a^{**}) = a^{**}$}
                \UnaryInfC{$(a) \hor (0) = a^{**}$}
                \UnaryInfC{$a = a^{**}$}
                \UnaryInfC{$a \in Reg(H)$}
            \end{prooftree}
        \end{subproof}

        \begin{subproof} $a\leq b\rar b^{*}\leq a^{*}$. %A<B -> B*<A*
            \begin{prooftree}
                \AxiomC{$a\leq b$}

                \AxiomC{$\gamma = a^*\hnd b^*$}
                \UnaryInfC{$\gamma = (a\hop 0)\hnd(b\hop 0)$}
                \UnaryInfC{$\gamma = (a\hor b)\hop 0$}
                
                \BinaryInfC{$\gamma = b\hop 0$}
                \UnaryInfC{$\gamma = b^*$}
            \end{prooftree}
        \end{subproof}

        \begin{subproof} $a\leq b\rar a^{**}\leq b^{**}$. %A<B -> A**<B**
            \begin{prooftree}
                \AxiomC{$a\leq b$}
                \UnaryInfC{$a^*\geq b^*$}
                \UnaryInfC{$a^{**}\geq b^{**}$}
            \end{prooftree}
        \end{subproof}

        \begin{subproof} De Morgan 1.
            \begin{prooftree}
                \AxiomC{$\gamma = (a\hor b)^*$}
                \UnaryInfC{$\gamma = (a\hor b)\hop 0$}
                \UnaryInfC{$\gamma = (a\hop 0)\hnd(b\hop 0)$}
                \UnaryInfC{$\gamma = a^*\hnd b^*$}
            \end{prooftree}
        \end{subproof}

        \begin{subproof} De Morgan 2 (Enfraquecida).
            \begin{prooftree}
                \AxiomC{$\gamma = (a^*\hnd b^*)^*$}
                \UnaryInfC{$\gamma = ((a\hop 0)\hnd(b\hop 0))^*$}
                \UnaryInfC{$\gamma = ((a\hor b)\hop 0)^*$}
                \UnaryInfC{$\gamma = (a\hor b)^{**}$}
            \end{prooftree}
        \end{subproof}

        \begin{subproof} $a^{***}=a^*$.  %a*** = a*
            \begin{prooftree}
                \AxiomC{$\gamma = a^{***}\hnd a^*$}
                \UnaryInfC{$\gamma = (a^{**}\hop)\hnd (a\hop 0)$}
                \UnaryInfC{$\gamma = (a^{**}\hor a)\hop 0)$}
                \UnaryInfC{$\gamma = a^{**}\hop 0$}
                \UnaryInfC{$\gamma = a^{***}$}
                \UnaryInfC{$a^{***}\hnd a^* = a^{***}$}
                \UnaryInfC{$a^{***}\leq a^*$}
                
                \AxiomC{$a^{*}\leq (a^*)^{**}= a^{***}$}
                
                \BinaryInfC{$a^{***}= a^*$}
            \end{prooftree}
        \end{subproof}
        \begin{subproof} Ter complemento implica ser Complementado.
            \begin{prooftree}
                \AxiomC{$c$ $comp$ $a$}

                \AxiomC{$c$ $comp$ $a$}
                \UnaryInfC{$c\hor a= 1$}

                \AxiomC{$c$ $comp$ $a$}
                \UnaryInfC{$c\hnd a = 0$}
                \UnaryInfC{$c\leq a^*$}

                \BinaryInfC{$a\hor (c\hnd a^*) = 1$}
                \UnaryInfC{$(a\hor c)\hnd (a\hor a^*)= 1$}

                \BinaryInfC{$a\hor a^* = 1$}
            \end{prooftree}
        \end{subproof}

        \begin{subproof} Complemento é único.
            \begin{prooftree}
                \AxiomC{$c$ $comp$ $a$}
                \UnaryInfC{$c \hor a = 1\land c \hnd a = 0$}
                
                \AxiomC{$c'$ $comp$ $a$}
                \UnaryInfC{$c'\hor a = 1\land c'\hnd a = 0$}
                
                \AxiomC{$c = c\hnd 1$}
                \BinaryInfC{$c = c\hnd (c'\hor a)$}
                \UnaryInfC{$c = (c\hnd c')\hor(c\hnd a)$}
                \BinaryInfC{$c = (c\hnd c')\hor 0$}
                \UnaryInfC{$c = c\hnd c'$}
                \UnaryInfC{$c \leq c'$}
            \end{prooftree}
            Como as situações de $c$ e de $c'$ são simétricas, vale que:
            \begin{prooftree}
                \AxiomC{$c'\leq c$}
            \end{prooftree}
            E, portanto,
            \begin{prooftree}
                \AxiomC{$c'= c$}
            \end{prooftree}
            
        \end{subproof}
        \begin{subproof} $a^*\hor b\leq a\hop b$ 
            \begin{prooftree}
                \AxiomC{$\gamma = (a^*\hor b) \hnd (a\hop b)$}
                \UnaryInfC{$\gamma = ((a\hop 0)\hnd(a\hop b))\hor b$}
                \UnaryInfC{$\gamma = (a\hop (0\hnd b))\hor b$}
                \UnaryInfC{$\gamma = (a\hop 0)\hor b$}
                \UnaryInfC{$\gamma = (a^*\hor b)$}
            \end{prooftree}
        \end{subproof}
    \end{proof}
\end{theorem}

Munidos com estes resultados gerais, 
iremos tratar de álgebras de Boole.

\begin{definition}{\footnote{Há definições alternativas, provaremos equivalências mais tarde.}Álgebras Booleanas}
        $B = \TUPLE{\bb{B}}{\leq}$\footnote{Novamentes, omitimos $\hop$, $1$, $\hnd$ \etc.}
        é dita uma álgebra booleana se, e só se, vale o seguinte:
    \begin{enumerate}
        \item $B$ é uma Álgebra de Heyting.
        \item $\bb{B} = Comp(B)$.
    \end{enumerate}
\end{definition}

A motivação dessa definição é nos 
aproximarmos da lógica clássica onde
$\yields\psi\lor\neg\psi$.

\begin{theorem}{Resultados sobre álgebras de Boole}
    Seja $B=\TUPLE{\bb{B}}{\leq}$ Álgebra Booleana, e sejam $a,b,c\in\bb{B}$.

    \begin{enumerate}
        \item $(a\hnd b)^* = a^*\hor b^*$.: De Morgan 2
        \item $a\hor a^* = 1$.
    \end{enumerate}

    \begin{proof}
        \begin{subproof}De Morgan 2:
            \begin{prooftree}
                \AxiomC{$(a\hor b)^*= (a^{**}\hor b^{**})^*$}
                \UnaryInfC{$(a\hor b)^*= a^{***}\hnd b^{***}$}
                \UnaryInfC{$(a\hor b)^*= a^{*}\hnd b^{*}$}
            \end{prooftree}
        \end{subproof}

        \begin{subproof}Meio excluído
            \begin{prooftree}
                \AxiomC{$\bb{B} = Comp(B)$}
                \UnaryInfC{$\bb{B} = Reg(B)$}
                \UnaryInfC{$\forall\alpha\in\bb{B}:\alpha = \alpha^{**}$}
                
                \AxiomC{$\gamma = 0$}
                \UnaryInfC{$\gamma = a\hnd a^*$}
                \UnaryInfC{$\gamma^* = (a\hnd a^*)^*$}
                \UnaryInfC{$\gamma^* = a^*\hor a^{**}$}
                \BinaryInfC{$\gamma^* = a^*\hor a$}
                \UnaryInfC{$0^* = a^*\hor a$}
                \UnaryInfC{$1 = a^*\hor a$}
            \end{prooftree}
        \end{subproof}
    \end{proof}
\end{theorem}

Como esperávamos, temos ambas a remoção
da dupla negação e o meio excluído (bem
como De Morgan, com força total), que 
conhecemos bem da lógica Booleana.

Vale resaltar que existem muitas outras
álgebras Booleanas além da do $2$, a 
álgebra do $4$ (ou $\wp(2)$):

\begin{minipage}{\textwidth}
    \begin{center}
        \begin{tikzpicture}[remember picture]
            \node (one) at (0, 0.5)                     {$1$};
            \node (a)   [below of= one] [left  of= one] {$a$};
            \node (b)   [below of= one] [right of= one] {$b$};
            \node (nul) [below of= a  ] [right of= a]   {$0$};
            
            \draw [->] (nul) -- (a); 
            \draw [->] (nul) -- (b);
            \draw [->] (a) -- (one);
            \draw [->] (b) -- (one);
        \end{tikzpicture}
    \end{center}
\end{minipage}

Este diagrama de Hasse representa nossa
álgebra e não é difícil verificar que 
todos os seus elementos são complementado. 
Assim, podemos concluir que ela é uma 
álgebra Booleana.